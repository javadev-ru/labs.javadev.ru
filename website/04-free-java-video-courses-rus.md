---
layout: page
title: Материалы по Java, свободно распространяемые в сети !!!
permalink: /free-java-video-courses-rus/
---


# Материалы по Java, свободно распространяемые в сети !!!

Т.е. сам автор выложил материалы в сеть.

Сразу магнетлинки. Кто не знает, магнетлинк (строка с хешем) копируется в буфер и вставляется в торрент клиент как url. Остается подождать когда клиент начнет качать.

<br/>

**[Мирончик Игорь Янович] Курс программирования на языке Java. Начальный уровень. Киев 11-15 апреля 2011г. [2011, RUS]**

980B8587E4B51233A878CB8785DF42EE7828B7CC

<br/>

**[Мирончик Игорь] JAVA EE: ECLIPSE, JBOSS 7 и EJB 3 [2012, RUS]**

3CB7FB3E67495C4479AEE5822F7C402A920B3BC3


<br/>

**[KharkovITCourses] junior java developer часть.1 [2013, RUS]**

685A326F4746A3A8ADECE966EEBA8C0ED9FC3A4A


<br/>

**[KharkovITCourses] junior java developer часть.2 [2013, RUS]**

2EA86AC5257FCAD2D42F0076B07E3F5CC950CC08



<br/>

**[KharkovITCourses] Java Core часть.1 [2013 - 2014, RUS]**

A8DDA9A4EAA94B4099C349929F307A307B23D5C1



<br/>

**[KharkovITCourses] Java Core часть.2 [2013 - 2014, RUS]**

E50B0FBBA3D15871DD6215ECF13E589D1EB3C241


<br/>

**[Yakov Fain] - Java Training Videos | Программирование с Java и Java EE [2014, ENG, RUS]**

FCFC5D84F813108F10533C3C535C58CCF844463D
