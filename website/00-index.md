---
layout: page
title: Java лабораторные исследования!
permalink: /
---

# Java лабораторные исследования!

**labs.javadev.ru** - позиционируется как сайт, на котором изучаются всевозможные книги и видеоматериалы по JAVA. Желающие могут подключиться к разбору материалов или начать новую тему.

Добавлять, вносить правки можно к любому курсу, постепенно улучшая содержание.

Материалы на английском языке, также планируется разбирать <a href="//labs.javadev.org/">здесь</a>

<br/>

<div align="center">
    <img src="/img/turtles.jpg" alt="Marley" border="0">
</div>

---

<br/>

<h3>Если кто собарает, покупает, меняет всевозможные курсы и электронные книги по Java на русском и английском языке</h3>

Напишите чем можете поделиться.  
Может найдется что-то интересное и для вас.

<img src="https://img.fotografii.org/a3333333mail.gif" alt="Marley" border="0">

<br/>

### Материалы по Java с YouTube !!!

<br/>

<div align="center">
    <a href="/beginners/">Для новичков</a> ||
    <a href="/devtools/">Java Development Tools</a> ||
    <a href="/spring/">Spring</a> ||
    <a href="/multithreading/">Многопоточность</a> ||
    <a href="/code-quality">Code Quality</a> ||
    <a href="/design-patterns/">Шаблоны проектирования</a> ||
    <a href="/logging/">Логирование</a>
</div>

<br/>

### Материалы по Java, свободно распространяемые в сети !!!

(Public Domain или как там это называется)

<br/>

<div align="center">
    <img src="/img/tmemes/learn-java.png" alt="Игра для java разработчиков!" border="0">
</div>

<a href="/free-java-video-courses-rus/">Вот</a>

<br/>

### Список бесплатных видеокурсов для изучения JAVA на русском языке

<a href="/library/java/free/">здесь</a>

<hr/>

<br/>

### Список платных видеокурсов для изучения JAVA на русском языке (только описание)

<a href="/library/java/paid/">здесь</a>

<br/>

### Java Case Study от Роберта Мартина (От автора "Чистый код")

UPD. Оказалось очень нудно смотреть. При этом Роберт Мартин такой говорит, "Я тут дома посидел и без камеры нарефакторил. <br/>
Разбираться не будем, далее работаем вот с этим коммитом". <br/>
Может позже вернусь.

Если найду время, постараюсь посмотреть.  
Материал должен быть интересным для >= middle java разработчиков.

В видео создается новый web проект на java.

<br/>

UPD. Решили посмотреть коллективно этот материал, чтобы мжно было при необходимости обсудить. <br/>
Если найдутся желающие присоединиться, качайте материалы, присоединяйтесь к нашему чату. <br/><br/>

https://gitter.im/javadev-org/Lobby

<br/>

Для входа требуется github аккаун, впрочем аккаунт твиттера тоже подойдет.

<br/>

src:  
https://github.com/cleancoders/CleanCodeCaseStudy

fitnesse app:
http://fitnesse.org

Чтобы это все запустить как у Мартина, делаем так:

1. git clone https://github.com/cleancoders/CleanCodeCaseStudy

2. git reset --hard 9caa79cbba6257ac83d803060911a14b31ac3473

3. Переименовываем проект в cleancoderscom

4. Открываем проект в IDEA.

a) File --> Project Structure

Project language level: 8

Project compilation output: /home/marley/projects/cc/cleancoderscom/out

b) Modules указать папку с исходниками.

<br/>

Далее:

Копирую fitnesse-standalone.jar в корень проекта.

Запускаю его командой:

    $ java -jar fitnesse-standalone.jar -p 8080

<br/>
    
Подключаюсь:
    
    http://localhost:8080/CleanCoders

<div align="center">

<img src="/img/cleancode/fitnesse.png" alt="Java Case Study fitnesse" border="0">

</div>

<br/>

Библиотеки junit-hierarchicalcontextrunner:

https://github.com/bechte/junit-hierarchicalcontextrunner/wiki

<br/>

### [Udemy] Building An E-Commerce Store Using Java Spring Framework !!!

https://www.udemy.com/building-an-e-commerce-store-using-java-spring-framework/

Появилось желание разобрать видеокурс по Spring MVC, где бы разбирался какой-никакой а проект.

Само видео не составило большого труда скачать.

Если будут желающие обсудить, вот <a href="https://bitbucket.org/marley-spring/building-an-e-commerce-store-using-java-spring-framework/src">github</a> в котором я планирую делать записи.

---

<br/>

### [JavaWebinar] Top Java v2 [RUS, 2015]

<a href="/courses/javawebinar/topjava/">здесь</a>

<br/>

### [Мирончик Игорь] JAVA EE: ECLIPSE, JBOSS 7 и EJB 3 [RUS, 2012]

<a href="/courses/mironchik/jboss-ejb-postgresql/">Начал разбирать здесь, но похоже силы оставили меня.</a>
