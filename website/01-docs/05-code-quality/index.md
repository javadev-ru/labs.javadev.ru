---
layout: page
title: Code Quality
permalink: /code-quality/
---


### Code Quality

https://www.youtube.com/watch?v=sxUqYASeRzU

<br/>

### Немного Теории (на украинском, но впринципе понятно)

Буду признателен за ссылки на такого же рода материалы на русском. Меня они больше всего интересуют.

Может и до меня когда-нибудь дойдет и я смогу запомнить, что еще за Leskov Principles.

<ul>
    <li><a href="https://www.youtube.com/watch?v=D-VoYhAex0E">EPAM_JaMP_Development Principles: DRY, SOLID, YAGNI, KISS_by Maksym Oleshchuk</a></li>
    <li><a href="https://www.youtube.com/watch?v=du-1S5dd1_Y">EPAM_JaMP_Software Development Philosophies_by Maksym Oleshchuk</a></li>
    <li><a href="https://www.youtube.com/watch?v=OhilxE4_KMc">EPAM_JaMP_Object Oriented Analysis and Design_by Maksym Oleshchuk</a></li>
    <li><a href="https://www.youtube.com/watch?v=sIxGG7pQuSA">EPAM_JaMP_Clean Code. AntiPatterns_by Maksym Oleshchuk</a></li>
</ul>


<br/>

<strong>SOLID:</strong>

<ul>
    <li><strong>S</strong>ingle responsibility</li>
    <li><strong>O</strong>pen/Closed</li>
    <li><strong>L</strong>iskov substitution</li>
    <li><strong>I</strong>nterface segregation</li>
    <li><strong>D</strong>ependency inversion</li>
</ul>

<br/>

DRY: (don't repeat yourself)

KISS (Keep it simple, stupid)

YAGNI (You aren't gonna need it)

<br/>

PS. Видео от Роберта Мартина Clean Code, лежит на рутрекере. Есть книга на русском. Ее следует всем прочитать. И больше никогда не писать тупые комментарии в коде.
