---
layout: page
title: Java Development Tools
permalink: /devtools/
---

<br/>

# Java Development Tools

<a href="//javadev.org/devtools/repository-management/">Sonatype Nexus (Свой репозиторий)</a>  
<a href="//javadev.org/devtools/cicd/jenkins/">Jenkins </a>  
<a href="//javadev.org/devtools/code-quality/sonarqube/installation">SonarQube (Синтаксический анализ кода. Показывает процент повторяемости, возможные ошибки и другую полезную информацию)</a>  
<a href="//javadev.org/devtools/testing/">Selenium grid</a>
