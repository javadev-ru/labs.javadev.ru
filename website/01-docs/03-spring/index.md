---
layout: page
title: Java Spring
permalink: /spring/
---


### Java Spring

<br/>


### [Udemy] Building An E-Commerce Store Using Java Spring Framework !!!


https://www.udemy.com/building-an-e-commerce-store-using-java-spring-framework/


Появилось желание разобрать видеокурс по Spring MVC, где бы разбирался какой-никакой да проект.

Само видео не составило большого труда скачать.

Передам фалы тому, кто создаст раздачу, например, на darkos(.)club. На рутрекере меня "забанили", поэтому я больше не способствую его развитию.

<br/>

PS. Вот по этой <a href="http://pastebin.com/raw/HyESbEeJ">магнет ссылке</a>, можно его скачать.

Мои <a href="https://bitbucket.org/marley-spring/building-an-e-commerce-store-using-java-spring-framework">исходники проекта</a>, по мере прохождения курса будут пополняться.


Возможно, поздее нужно будет еще посмотреть. <a href="http://pastebin.com/raw/eE07KHer">Udemy - Java Spring MVC Framework with AngularJS by Google and HTML5</a>,



<br/><br/>

<hr/>


<h3>Приложение Spring Pet Clinic</h3>


<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/rJZHerwi8R0" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

### Евгений Борисов — Spring Puzzlers: тонкости и нюансы работы Spring

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/U8MtGYa04v8" frameborder="0" allowfullscreen></iframe>
</div>
