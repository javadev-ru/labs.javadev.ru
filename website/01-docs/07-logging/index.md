---
layout: page
title: Логирование
permalink: /logging/
---

<br/>

### Владимир Красильщик – Что надо знать о логировании прагматичному Java‑программисту

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/qzqAUUgB3v8" frameborder="0" allowfullscreen></iframe>
</div>

<br/>
<br/>

Если коротко: SLF4J + LOGBack


<br/>


### Логирования в Java (SLF4J, LOG4J, Logback)

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/Lyqc8HicPMM" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>
