---
layout: page
title: Для новичков
permalink: /beginners/
---

<br/>


<br/>

### Видео курс Java Starter (2016)

<br/>


magnet:?xt=urn:btih:de3136c7312fe0c84a256e65b8f7c7d27606d1ba&tr=http%3A%2F%2Fbt.dark-os.com%3A2710%2F000143ffd233eb7879480b6d898c7746%2Fannounce



<br/>

### Видеокурсы по Java от Вячеслава Ковалевского

<br/>

    1. Введение в Java
    2. Java: Структуры данных
    3. Байт-код Java
    4. Java Generics

<br/>

magnet:?xt=urn:btih:2F1C92C0E1E0679F3F3CB8F31D7CC336848D62C9&tr=http%3A%2F%2Fbt.t-ru.org%2Fann%3Fmagnet



<br/>

### ТОП-5 русскоязычных каналов YouTube для изучения Java (По мнению Сергея Лугового)

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/1-n3Pf8ZF5g" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

По мне, так лучше надыбать видео Антона Сабурова - Java SE/EE или Григория Кислина.

<br/>
<br/>

### Java. Путь от ученика до эксперта

Вот такая ссылка есть:  
https://github.com/peterarsentev/java-courses

<br/>
<br/>

### [Технострим Mail.Ru Group] Разработка на JAVA. Лекция 1

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/WfIS-0jqXJ0" frameborder="0" allowfullscreen></iframe>
</div>
