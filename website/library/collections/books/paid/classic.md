---
layout: page
title: Classic Java Book
permalink: /library/books/classic/
---


<ul>
	<li style="color:green"><strong>[Herbert Schildt] Java: The Complete Reference (Ninth Edition)</strong></li>
	<li><strong>[Cay S. Horstmann] Core Java Volume I - Fundamentals + Core Java, Volume II -Advanced Features</strong></li>
	<li><strong>[Paul Deitel and Harvey Deitel] Java How To Program (10th Edition)</strong></li>
</ul>
