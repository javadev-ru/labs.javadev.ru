---
layout: page
title: Ссылки на описание платных видеокурсов по java на русском языке
permalink: /library/java/paid/
---

<a href="/library/javawebinar/java/paid/">javawebinar</a>  
<a href="/library/javabegin/java/paid/">javabegin</a>  
<a href="/library/prog.kiev/java/paid/">prog.kiev</a>  
<a href="/library/prog-school/java/paid/">prog-school</a>  
<a href="/library/luxoft/java/paid/">luxoft</a>  
<a href="/library/geekbrains/java/paid/">Geekbrains</a>  
<a href="/library/foxford/java/paid/">Foxford</a>  
<a href="/library/java-course/java/paid/">java-course</a>
