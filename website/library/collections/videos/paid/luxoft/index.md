---
layout: page
title: Luxoft
permalink: /library/luxoft/java/paid/
---


<a href="http://luxoft.ru/">Обзор JAVA-технологий разработки ПО</a>  

___

<a href="http://www.luxoft-training.ru/kurs/shablony_proektirovaniya_gof_redaktsiya_dlya_java.html/">Шаблоны проектирования (GoF). Редакция для Java </a>  


Описание:  

Паттерны (patterns, шаблоны) представляют собой консистенцию некоего опыта, пригодную для повторного использования. Паттерны находят применение во всех областях деятельности, поскольку позволяют использовать сработавшие ранее решения. Знание паттернов проектирования позволяет не только быстрее строить Ваши решения и получать качественный исходный код, но и проще общаться с Вашими коллегами, которые уже освоили данную технологию.  

В курсе рассматриваются базовые 23 паттерна, изложенные в книге "Design Patterns: Elements of Reusable Object-Oriented Software" by Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides (Gang of Four, GoF).
