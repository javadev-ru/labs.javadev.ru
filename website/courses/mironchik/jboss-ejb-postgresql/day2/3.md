---
layout: page
title: Распечатать дерево JNDI в браузере
permalink: /courses/mironchik/jboss-ejb-postgresql/3/
---


S1.java

{% highlight java %}

package ru.javalabs;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns="/s1", loadOnStartup=10)
public class S1 extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public S1() {
        super();

    }

    private long id;

	@Override
	public void destroy() {
		System.out.println("---- Servlet id=" + id + "created");
		super.destroy();
	}


	@Override
	public void init() throws ServletException {
		super.init();

		id = System.currentTimeMillis();
		System.out.println("---- Servlet id=" + id + "created");
	}


	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<html>");

		Context ctx = null;

		try {
			ctx = new InitialContext();
			NamingEnumeration ne = ctx.list("");

			while (ne.hasMoreElements()){
				Object o = (Object) ne.nextElement();
				out.println("<hr/> " + o);
			}

		} catch (Exception e){
			out.println("<hr/>Error " + e);
		} finally {
			try {
				ctx.close();
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		out.println("</html>");
	}
}


{% endhighlight %}


web.xml

{% highlight xml %}

<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  <display-name>View</display-name>
  <welcome-file-list>
    <welcome-file>s1</welcome-file>
  </welcome-file-list>
</web-app>

{% endhighlight %}



application.xml

{% highlight xml %}

<?xml version="1.0" encoding="UTF-8"?>
<application xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/application_6.xsd" version="6">
  <display-name>App1</display-name>
  <module>
    <web>
      <web-uri>View.war</web-uri>
      <context-root>view</context-root>
    </web>
  </module>
</application>


{% endhighlight %}



____


Вариант с печатью в JNDI своей переменной и ее инкремент по F5  


S1.java

{% highlight java %}

package ru.javalabs;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/s1", loadOnStartup=10)
public class S1 extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public S1() {
        super();

    }

    private long id;

	@Override
	public void destroy() {
		System.out.println("---- Servlet id=" + id + "created");
		super.destroy();
	}


	@Override
	public void init() throws ServletException {
		super.init();

		id = System.currentTimeMillis();
		System.out.println("---- Servlet id=" + id + "created");
	}


	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<html>");

		Context ctx = null;
		Long c = 0L;

		try {
			ctx = new InitialContext();

			try {
				ctx.bind("counter", c);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			NamingEnumeration ne = ctx.list("");

			while (ne.hasMoreElements()){
				Object o = (Object) ne.nextElement();
				out.println("<hr/> " + o);
			}

			c = (Long) ctx.lookup("counter");
			c++;
			ctx.rebind("counter", c);

		} catch (Exception e){
			out.println("<hr/>Error " + e);
		} finally {
			try {
				ctx.close();
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		out.println("</html>");
	}

}

{% endhighlight %}
