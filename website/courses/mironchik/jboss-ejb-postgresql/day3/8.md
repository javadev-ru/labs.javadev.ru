---
layout: page
title: EJB Timer
permalink: /courses/mironchik/jboss-ejb-postgresql/8/
---

Создаем Enterprise проект (App1).  
Создаем EJB проект (Model). В этот раз с Crete an EJB Client JAR...  


Model Client --> Build Path --> Configure Build Path --> Java Build Path --> Libraries --> Add Library --> Java EE 5 Libraries



Создаем Dynamic WebProject проект (View).  

WebProject --> build path --> Projects --> Add --> ModelClient


====

java.lang.UnsupportedClassVersionError  

Model Client --> Properties --> Java Compiler --> Compiler compliance level: 1.7



http://localhost/view/s1  
http://localhost/view/s2  

Результаты в консоли:


    02:33:20,731 INFO  [stdout] (EJB default - 1) -----------Wed Apr 15 02:33:20 UTC 2015
    02:33:22,728 INFO  [stdout] (EJB default - 2) -----------Wed Apr 15 02:33:22 UTC 2015
    02:33:24,724 INFO  [stdout] (EJB default - 3) -----------Wed Apr 15 02:33:24 UTC 2015
    02:33:26,739 INFO  [stdout] (EJB default - 4) -----------Wed Apr 15 02:33:26 UTC 2015
    02:33:28,737 INFO  [stdout] (EJB default - 5) -----------Wed Apr 15 02:33:28 UTC 2015
    02:33:30,725 INFO  [stdout] (EJB default - 6) -----------Wed Apr 15 02:33:30 UTC 2015
    02:33:32,730 INFO  [stdout] (EJB default - 7) -----------Wed Apr 15 02:33:32 UTC 2015
    02:33:34,736 INFO  [stdout] (EJB default - 8) -----------Wed Apr 15 02:33:34 UTC 2015
    02:33:36,734 INFO  [stdout] (EJB default - 9) -----------Wed Apr 15 02:33:36 UTC 2015
    02:33:38,731 INFO  [stdout] (EJB default - 10) -----------Wed Apr 15 02:33:38 UT
