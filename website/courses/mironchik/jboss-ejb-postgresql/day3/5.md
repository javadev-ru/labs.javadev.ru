---
layout: page
title: Создание EJB проекта c Remote интерфейсом
permalink: /courses/mironchik/jboss-ejb-postgresql/5/
---

Создаем Enterprise проект (App1).
Создаем EJB проект (Model). Без EJB Client Jar и ejb-jar.xml

Отправляют проект на сервер:

    java:global/App1/Model/Facade!ru.javalabs.model.FacadeRemote
    java:app/Model/Facade!ru.javalabs.model.FacadeRemote
    java:module/Facade!ru.javalabs.model.FacadeRemote
    java:jboss/exported/App1/Model/Facade!ru.javalabs.model.FacadeRemote
    java:global/App1/Model/Facade!ru.javalabs.model.FacadeLocal
    java:app/Model/Facade!ru.javalabs.model.FacadeLocal
    java:module/Facade!ru.javalabs.model.FacadeLocal



Создаем Enterprise проект (AppWeb).
Создаем Dynamic WebProject проект (View).


Model -> Export -> Export (java jar) -> ModelClient.jar

View -> Properties -> Deployment Assembly -> Add -> Archive from file system -> ModelClient.jar


При обращении к  
http://localhost/view/s1


Получили:

Proxy for remote EJB StatelessEJBLocator{appName='App1', moduleName='Model', distinctName='', beanName='Facade', view='interface ru.javalabs.model.FacadeRemote'}

Hello world id = 1429012577387
