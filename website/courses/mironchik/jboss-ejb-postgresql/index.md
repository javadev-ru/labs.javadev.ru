---
layout: page
title: Мирончик Игорь JAVA EE ECLIPSE, JBOSS 7 и EJB 3 [2012, RUS]
permalink: /courses/mironchik/jboss-ejb-postgresql/
---


Материалы:  
http://rutracker.org/forum/viewtopic.php?t=3956884

<hr/>

Коды коллеги Oracle Database  
https://github.com/javalabs-ru/Mironchik_JAVA_EE_ECLIPSE_JBOSS_and_EJB3  

Мои Коды с попыткой работы с PostgreSQL Database
https://github.com/javalabs-ru/mironchik-eclipse-jboss-ejb  

<hr/>

В качестве базы, использовался не Oracle как в курсе, а PostgreSQL.


День 2:  

<strong><a href="/courses/mironchik/jboss-ejb-postgresql/1/">Подключение к базе данных PostgreSQL из java se приложения</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/2/">Пример жизненного цикла сервлета</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/3/">Распечатать дерево JNDI в браузере</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/4/">Подключение к базе данных PostgreSQL из сервера приложений jBoss 7.X</a></strong>


День 3:  

<strong><a href="/courses/mironchik/jboss-ejb-postgresql/5/">Создание EJB проекта c Remote интерфейсом</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/6/">Создание EJB проекта c Local интерфейсом</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/7/">Predestroy, Postconstruct, Session Context</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/8/">EJB Timer</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/9/">Асинхронный вызов сессионных бинов</a></strong>


День 4:


<strong><a href="/courses/mironchik/jboss-ejb-postgresql/10/">StateFul бин</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/11/">StateFul бин пример 2</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/12/">Singleton</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/13/">Persistance</a></strong>  
<strong><a href="/courses/mironchik/jboss-ejb-postgresql/14/">XA Transactions</a></strong>  


День 5:

Не стал делать.

JPA  
JMS  
WebServices  
