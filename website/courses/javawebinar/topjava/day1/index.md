---
layout: page
title: JavaWebinar - Top Java v2 - день 1
permalink: /courses/javawebinar/topjava/1/
---

### Spring PetClinic (Базовое приложение)
https://github.com/spring-projects/spring-petclinic

1) Копируем исходные коды проекта


    $ git clone https://github.com/JavaWebinar/topjava02
    $ cd topjava02/


    // b86caa5	Switch project to war packaging
    $ git reset --hard b86caa5

    $ git remote -v
    origin	https://github.com/JavaWebinar/topjava02 (fetch)
    origin	https://github.com/JavaWebinar/topjava02 (push)


PS. 61266b5	Test change тот коммит с которым начинает работать автор.


Создал репо topjava

    $ git remote rm origin
    $ git remote add origin https://github.com/javalabs-ru/topjava.git
    $ git push origin master


# IDE IDEA

Установлены Plugins:

Settings --> Plugins

Maven Integration
Maven Integration Extension


Ctrl + q - java docs
Ctrl + b



Ctrl + Alt + Shift + S
SDKs --> Documentation Path --> Specify Url
После этого по Shift + F1 можно будет получать доку в браузере.


### Для работы с TomCat в web админке.


Добавляем в файл: $TOMCAT_HOME/conf/tomcat-users.xml


перед закрывающим тегом </tomcat-users> следующую строку:


    <user username="tomcat" password="tomcat" roles="tomcat,manager-gui,admin-gui"/>


Стартуем TomCat в DebugMod  

    ./catalina.sh jpda start


Админка:  
http://localhost:8080/manager

8000 - порт debug по умолчанию.


### Dependency для работы с сервлетами.

В pom.xml добавили:


    <!-- Web -->
    <dependency>  
        <groupId>javax.servlet</groupId>
        <artifactId>servlet-api</artifactId>
        <version>2.5</version>
        <scope>provided</scope>
    </dependency>


Commit: Add servlet and jsp/html



### Добавление TomCat Server в IDEA.

Run --> Edit Configuration --> Plus --> Tomcat Server --> Local

Warning. No artifacts marked for deployment.

Fix -->

Select artifact to deploy: topjava:war exploded (распакованная в виде каталога)  
Application context: /topjava


http://localhost:8080/topjava/  
http://localhost:8080/topjava/userList.jsp


Для автоматического обновления данных на сервере при изменении исходного кода страницы.  
Server --> Deployment --> Update resources on frame Deactivation


http://localhost:8080/topjava/users

Commit: Change forward to redirect


### Логгирование.

pom.xml

    <properties>

    ***

        <!-- Logging -->
        <logback.version>1.1.2</logback.version>
        <slf4j.version>1.7.7</slf4j.version>

    </properties>

    ***

    <dependencies>

    ***

        <!-- Logging with SLF4J & LogBack -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>${slf4j.version}</version>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jul-to-slf4j</artifactId>
            <version>${slf4j.version}</version>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
            <scope>runtime</scope>
        </dependency>


    </dependencies>



___

Добавили:


    package ru.javawebinar.topjava;

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    public class LoggerWrapper {

        private Logger logger;

        public LoggerWrapper(Logger logger) {
            this.logger = logger;
        }

        public static LoggerWrapper get(Class aClass) {
            return new LoggerWrapper(LoggerFactory.getLogger(aClass));
        }

        public void debug(String msg) {
            logger.debug(msg);
        }

        public void info(String msg, Object... arguments) {
            logger.info(msg, arguments);
        }

        public void warn(String msg) {
            logger.warn(msg);
        }

        public void warn(String msg, Throwable t) {
            logger.warn(msg, t);
        }

        public void error(String msg) {
            logger.error(msg);
        }

        public void error(String msg, Throwable t) {
            logger.error(msg, t);
        }

        public boolean isDebug() {
            return logger.isDebugEnabled();
        }

        public IllegalStateException getIllegalStateException(String msg) {
            return getIllegalStateException(msg, null);
        }

        public IllegalStateException getIllegalStateException(String msg, Throwable e) {
            logger.error(msg, e);
            return new IllegalStateException(msg, e);
        }

        public IllegalArgumentException getIllegalArgumentException(String msg) {
            return getIllegalArgumentException(msg, null);
        }

        public IllegalArgumentException getIllegalArgumentException(String msg, Throwable e) {
            logger.error(msg, e);
            return new IllegalArgumentException(msg, e);
        }

        public UnsupportedOperationException getUnsupportedOperationException(String msg) {
            logger.error(msg);
            return new UnsupportedOperationException(msg);
        }
    }


Создали:  
src/main/resources/logback.xml

${TOPJAVA_ROOT} - корень проекта, переменная должна быть определена в переменных окружения.


    <?xml version="1.0" encoding="UTF-8"?>
    <configuration scan="true" scanPeriod="30 seconds">

        <contextListener class="ch.qos.logback.classic.jul.LevelChangePropagator">
            <resetJUL>true</resetJUL>
        </contextListener>

        <!-- To enable JMX Management -->
        <jmxConfigurator/>

        <appender name="file" class="ch.qos.logback.core.FileAppender">
            <file>${TOPJAVA_ROOT}/log/topjava.log</file>

            <encoder>
                <charset>UTF-8</charset>
                <pattern>%date %-5level %logger{0} [%file:%line] %msg%n</pattern>
            </encoder>
        </appender>

        <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
            <encoder>
                <charset>UTF-8</charset>
                <pattern>%-5level %logger{0} [%file:%line] %msg%n</pattern>
            </encoder>
        </appender>

        <logger name="ru.javawebinar.topjava" level="DEBUG"/>

        <root level="INFO">
            <appender-ref ref="file"/>
            <appender-ref ref="console"/>
        </root>
    </configuration>


Создали каталог для логов в корне проекта с именем log


Сервлет стал выглядеть вот так:


    package ru.javawebinar.topjava.web;

    import ru.javawebinar.topjava.LoggerWrapper;
    import java.io.IOException;

    public class UserServlet extends javax.servlet.http.HttpServlet {

        private static final LoggerWrapper LOG = LoggerWrapper.get(UserServlet.class);

        protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
            LOG.debug("redirect to userList");
            response.sendRedirect("userList.jsp");
        }
    }

<br/>

    $ mkdir /home/marley/Desktop/_DEV/kislin/topjava02/log/
    $ export TOPJAVA_ROOT=/home/marley/Desktop/_DEV/kislin/topjava02
    $ echo ${TOPJAVA_ROOT}

IDEA restart  


http://localhost:8080/topjava/users


В консоли и в файле с логами:

    DEBUG UserServlet [LoggerWrapper.java:19] redirect to userList


Commit: Add logging
