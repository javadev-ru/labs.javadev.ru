---
layout: page
title: JavaWebinar - Top Java v2 - день 5
permalink: /courses/javawebinar/topjava/5/
---


### TomCat




    <tomcat.version>7.0.57</tomcat.version>

    ***

    <dependency>
        <groupId>org.apache.tomcat</groupId>
        <artifactId>tomcat-servlet-api</artifactId>
        <version>${tomcat.version}</version>
        <scope>provided</scope>
    </dependency>


### HSQL DB


    <profiles>
        <profile>
            <id>hsqldb</id>
            <dependencies>
                <dependency>
                    <groupId>org.hsqldb</groupId>
                    <artifactId>hsqldb</artifactId>
                    <version>2.3.2</version>
                </dependency>
            </dependencies>
        </profile>
        <profile>
            <id>postgres</id>
            <dependencies>
                <dependency>
                    <groupId>org.postgresql</groupId>
                    <artifactId>postgresql</artifactId>
                    <version>9.4-1201-jdbc41</version>
                </dependency>
                <dependency>
                    <groupId>org.apache.tomcat</groupId>
                    <artifactId>tomcat-jdbc</artifactId>
                    <version>${tomcat.version}</version>
                </dependency>
            </dependencies>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
    </profiles>


### Spring Data JPA

    <spring-data-jpa.version>1.8.0.RELEASE</spring-data-jpa.version>

    <dependency>
        <groupId>org.springframework.data</groupId>
        <artifactId>spring-data-jpa</artifactId>
        <version>${spring-data-jpa.version}</version>
    </dependency>



### Кеширование с помощью encashe

    
