---
layout: page
title: JavaWebinar - Top Java v2 - день 4
permalink: /courses/javawebinar/topjava/4/
---

### Hibernate

spring-jdbc заменен на spring-orm


        <!-- Hibernate -->
        <hibernate.version>4.3.8.Final</hibernate.version>
        <hibernate-validator.version>5.1.3.Final</hibernate-validator.version>

        ***

        <!--DataBase-->
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>9.4-1201-jdbc41</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <!-- Hibernate -->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-entitymanager</artifactId>
            <version>${hibernate.version}</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>${hibernate-validator.version}</version>
        </dependency>
