# [labs.javadev.ru](https://labs.javadev.ru) source codes

<br/>

### Run labs.javadev.ru on localhost

    # vi /etc/systemd/system/labs.javadev.ru.service

Insert code from labs.javadev.ru.service

    # systemctl enable labs.javadev.ru.service
    # systemctl start labs.javadev.ru.service
    # systemctl status labs.javadev.ru.service

http://localhost:4033
